using Alchemy4Tridion.Plugins;
using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Xml.Linq;
using Tridion.ContentManager.CoreService.Client;
using System.Web;
using System.ServiceModel;
using System.Threading;
using System.Collections.Generic;
using System.Linq;
using Tridion.ContentManager;

namespace WhereUsedPlus.Controllers
{
    /// <summary>
    /// A WebAPI web service controller that can be consumed by your front end.
    /// </summary>
    /// <remarks>
    /// The following conditions apply:
    ///     1.) Must have AlchemyRoutePrefix attribute. You pass in the type of your AlchemyPlugin (the one that inherits AlchemyPluginBase).
    ///     2.) Must inherit AlchemyApiController.
    ///     3.) All Action methods must have an Http Verb attribute on it as well as a RouteAttribute (otherwise it won't generate a js proxy).
    /// </remarks>
    [AlchemyRoutePrefix("WhereUsedPlusService")]
    public class WhereUsedPlusServiceController : AlchemyApiController
    {
        // GET /Alchemy/Plugins/HelloExample/api/WhereUsedPlusService/UsingAndUsedItems/tcm/title
        /// <summary>
        /// Finds the list of items being used by a given Tridion object and the list of items using that
        /// object.
        /// </summary>
        /// <param name="tcm">
        /// The TCM ID of a Tridion object for which this function should find the using and used items in
        /// Tridion
        /// </param>
        /// <param name="title">
        /// The Title of a Tridion object for which this function should find the using and used items in
        /// Tridion
        /// </param>
        /// <returns>
        /// Formatted HTML containing the lists of using and used items for the input
        /// Tridion object
        /// </returns>
        [HttpGet]
        [Route("UsingAndUsedItems/{tcm}/{title}/{inPublicationContext}")]
        public string GetUsingAndUsedItems(string tcm, string title, bool inPublicationContext = false)
        {
            HashSet<string> uris = new HashSet<string>();
            var uri = "tcm:" + tcm;
            var pubUri = string.Empty;
            XElement usingItemsXml;
            XElement usedItemsXml;

            // Create a new, Core Service Client
            SessionAwareCoreServiceClient client = new SessionAwareCoreServiceClient("netTcp_2013");

            try
            {
                // Gets the current user so we can impersonate them for our client
                string username = GetUserName();
                client.Impersonate(username);

                // Creates a new UsingItemsFilterData
                UsingItemsFilterData usingFilter = new UsingItemsFilterData();
                // Sets the included versions for the filter to only retrieve the latest versions
                usingFilter.IncludedVersions = VersionCondition.OnlyLatestVersions;
                // Use our filter to retrieve all the items the object our TCM refers to is being used by
                usingItemsXml = client.GetListXml(uri, usingFilter);

                // Create a new filter for used items
                UsedItemsFilterData usedFilter = new UsedItemsFilterData();
                // Get all items using our Tridion object
                usedItemsXml = client.GetListXml(uri, usedFilter);
            }
            catch (Exception ex)
            {
                // proper way of ensuring that the client gets closed... we close it in our try block above,
                // then in a catch block if an exception is thrown we abort it.
                if (client != null)
                {
                    client.Abort();
                }

                // we are rethrowing the original exception and just letting webapi handle it
                throw ex;
            }

            if (inPublicationContext)
            {
                var obj = client.Read(uri, null);
                if (obj != null)
                {
                    // Get Context Publication
                    var pub = ((RepositoryLocalObjectData)obj).LocationInfo.ContextRepository;
                    if (pub != null)
                    {
                        pubUri = pub.IdRef;
                    }
                }
            }

            // create a list of items casted to context Publication
            if(!string.IsNullOrEmpty(pubUri))
            {
                foreach (var id in usingItemsXml.Elements().Select(el => el.Attribute("ID").Value))
                {
                    try
                    {
                        var contextUri = client.GetTcmUri(id, pubUri, null);
                        if (!string.IsNullOrEmpty(contextUri))
                        {
                            uris.Add(contextUri);
                        }
                    }
                    catch { }
                }
            }

            try
            {
                // We're done with our core service client so we close it now to free resources
                client.Close();
            }
            catch { }

            // Create a new string that will hold all the html we create to represent the information
            // we retrieved above
            string html = "";
            // The first thing we add is a title explaining the first set of results, in this
            // case the list of items using our object
            html += "<h2>" + title + " is used by:</h2>";


            // If didn't get any results we say so
            if ((inPublicationContext && uris.Count < 1) || !usingItemsXml.HasElements)
            {
                html += title + " is not being used by any items";
            }
            else
            {
                // Otherwise we create a div to hold all of our results
                html += "<div class=\"usingItems results\">";
                // Create the "table" heading with a helper function
                html += CreateItemsHeading();

                if (inPublicationContext)
                {
                    uris.ToList().ForEach(id => html += CreateItem(id));
                }
                else
                {
                    // Get all of the items as XElements
                    var usingItems = usingItemsXml.Elements();
                    // For each item in our results we call a CreateItem function to create the appropriate
                    // html
                    foreach (XElement item in usingItems)
                    {
                        html += CreateItem(item);
                    }
                }
                // Close the div we opened above
                html += "</div>";
            }
            // Similar to what we did above, we add a title explaining the second set of results,
            // all of the items our object is using
            html += "<h2>" + title + " uses:</h2>";
            // If we got no results here we display this information
            if (!usedItemsXml.HasElements)
            {
                html += title + " is not using any items";
            }
            else
            {
                // If we got results we create a div to hold them
                html += "<div class=\"usedItems results\">";
                // Then we grab all of our items as XElements
                var usedItems = usedItemsXml.Elements();
                // Use a function to create our "table" heading
                html += CreateItemsHeading();
                // For each item in our results we call a CreateItem function to create the appropriate
                // html
                foreach (XElement item in usedItems)
                {
                    html += CreateItem(item);
                }
                // Close the div we opened above
                html += "</div>";
            }
            // Return the html we've built.
            return html;
        }

        // GET /Alchemy/Plugins/HelloExample/api/WhereUsedPlusService/PagesWhereUsed/tcm/title/depth
        /// <summary>
        /// Finds any pages on which a Tridion object are used
        /// </summary>
        /// <param name="tcm">
        /// The TCM ID of a Tridion object for which this function should find any pages on which it is being
        /// used
        /// </param>
        /// <param name="title">
        /// The Title of a Tridion object for which this function should find any pages on which it is being
        /// used
        /// </param>
        /// <param name="depth">
        /// The number of connections we should traverse looking for pages on which an object is used, ex
        /// from one component to another component onto which it is component linked
        /// This prevents infinite loops, such as a component A linked to component B which is linked back
        /// to component A
        /// </param>
        /// <returns>
        /// Formatted HTML containing the list of pages using our Tridion object
        /// </returns>
        [HttpGet]
        [Route("PagesWhereUsed/{tcm}/{title}/{depth}/{inPublicationContext}")]
        public string GetPagesWhereUsed(string tcm, string title, int depth, bool inPublicationContext = false)
        {
            SessionAwareCoreServiceClient client = null;
            try
            {
                // Creates a new core service client
                client = new SessionAwareCoreServiceClient("netTcp_2013");
                // Gets the current user so we can impersonate them for our client
                string username = GetUserName();
                client.Impersonate(username);
                // Create a new string to hold the HTML we will use to display the pages using this component
                string html = "";
                // Call a function to get the pages HTML
                html = GetPages("tcm:" + tcm, client, html, depth, 1, inPublicationContext);
                // If the HTML we get from our function is empty it means no page is using our Tridion object
                // within the allowed number of links, so we say so.
                if (String.IsNullOrEmpty(html))
                {
                    html = title + "is not used on any pages";
                }
                else
                {
                    // If it isn't empty it means we do have some pages, so we create a heading to describe
                    // the results and add a div around them
                    string heading = "<h2>" + title + " is used on the following pages:</h2>";
                    heading += "<div class=\"results\">";
                    heading  += CreateItemsHeading();
                    html = heading + html + "</div>";
                }
                // We no longer need our core service client so we close it now to free resources
                client.Close();
                // Return the HTML representing the pages on which our Tridion object is used
                return html;
            }
            catch (Exception ex)
            {
                // proper way of ensuring that the client gets closed... we close it in our try block above,
                // then in a catch block if an exception is thrown we abort it.
                if (client != null)
                {
                    client.Abort();
                }

                // we are rethrowing the original exception and just letting webapi handle it
                throw ex;
            }
        }
        /// <summary>
        /// Recursive function which checks a Tridion object's using items for pages, then moves on to
        /// any using items using items to check for pages, etc.
        /// </summary>
        /// <param name="client">Tridion core service client to be used to look up the page using our object</param>
        /// <param name="count">The number of jumps we've used made from one item to another item using it</param>
        /// <param name="depth">The number of above jumps we should allow</param>
        /// <param name="html">The HTML string we take in and add our new html to</param>
        /// <param name="tcm">
        /// The TCM ID of the Tridion object we are checking for pages on which it is used, or of an item
        /// using this object
        /// </param>
        /// <returns>
        /// Formatted HTML containing the list of pages using our Tridion object, which is added to with each
        /// recursive call.
        /// </returns>
        public string GetPages(string tcm, SessionAwareCoreServiceClient client, string html, int depth, int count, bool inPublicationContext = false)
        {
            HashSet<string> uris = new HashSet<string>();
            var pubUri = string.Empty;

            // Create a UsingItemsFilter to get the items using the input Tridion object
            UsingItemsFilterData filter = new UsingItemsFilterData();
            // Sets the included versions for the filter to only retrieve the latest versions
            filter.IncludedVersions = VersionCondition.OnlyLatestVersions;
            // filter: Page only
            filter.ItemTypes = new[] { Tridion.ContentManager.CoreService.Client.ItemType.Page };
            // Get a list of all items using this object
            XElement usingItemsXml = client.GetListXml(tcm, filter);

            if (inPublicationContext)
            {
                var obj = client.Read(tcm, null);
                if (obj != null)
                {
                    // Get Context Publication
                    var pub = ((RepositoryLocalObjectData)obj).LocationInfo.ContextRepository;
                    if (pub != null)
                    {
                        pubUri = pub.IdRef;
                    }
                }
            }

            // create a list of items casted to context Publication
            if (!string.IsNullOrEmpty(pubUri))
            {
                foreach (var id in usingItemsXml.Elements().Select(el => el.Attribute("ID").Value))
                {
                    try
                    {
                        var contextUri = client.GetTcmUri(id, pubUri, null);
                        if (!string.IsNullOrEmpty(contextUri))
                        {
                            uris.Add(contextUri);
                        }
                    }
                    catch { }
                }
            }

            if (inPublicationContext)
            {
                uris.ToList().ForEach(id => html += CreateItem(id));
            }
            else
            {
                var usingItems = usingItemsXml.Elements();
                // For each of these items we check if they are a page
                foreach (XElement item in usingItems)
                {
                    if (item.Attribute("Type").Value.Equals("64") && !html.Contains(item.Attribute("ID").Value))
                    {
                        // If so we create a new item in our HTML
                        html += CreateItem(item);
                    }
                    else
                    {
                        // Otherwise we check if our count matches our depth, in which case we stop,
                        // otherwise we recursively call this function for the new item, incrementing our
                        // count by one
                        if (count != depth)
                        {
                            html = GetPages(item.Attribute("ID").Value, client, html, depth, count + 1, inPublicationContext);
                        }

                    }
                }
            }
            // Return the modified html string
            return html;
        }
        /// <summary>
        /// Creates an HTML representation of a Tridion object, including its title, path and TCM ID
        /// </summary>
        /// <param name="item">An XElement containing all information on a Tridion item</param>
        /// <returns>
        /// Formatted HTML presentation of key information for Tridion items
        /// </returns>
        public string CreateItem(XElement item){
            string html = "<div class=\"item\">";
            html += "<div class=\"icon\" style=\"background-image: url(/WebUI/Editors/CME/Themes/Carbon2/icon_v7.1.0.66.627_.png?name=" + item.Attribute("Icon").Value + "&size=16)\"></div>";
            html += "<div class=\"name\">" + item.Attribute("Title").Value + "</div>";
            html += "<div class=\"path\">" + item.Attribute("Path").Value + "</div>";
            html += "<div class=\"id\">" + item.Attribute("ID").Value + "</div>";
            html += "</div>";
            return html;
        }
        private string CreateItem(string id)
        {
            var html = string.Format("<!-- CreateItem:: id:{0} -->", id);
            SessionAwareCoreServiceClient client = null;
            try
            {
                client = new SessionAwareCoreServiceClient("netTcp_2013");
                client.Impersonate(GetUserName());
                var item = client.Read(id, null) as RepositoryLocalObjectData;
                html += "<div class=\"item\">";
                html += "<div class=\"icon\" style=\"background-image: url(/WebUI/Editors/CME/Themes/Carbon2/icon_v7.1.0.66.627_.png?name=" + GetIcon(item) + "&size=16)\"></div>";
                html += "<div class=\"name\">" + item.Title + "</div>";
                html += "<div class=\"path\">" + item.LocationInfo.Path + "</div>";
                html += "<div class=\"id\">" + item.Id + "</div>";
                html += "</div>";
                client.Close();
            }
            catch (Exception ex)
            {
                if (client != null)
                {
                    client.Abort();
                }
                html += string.Format("<!-- CreateItem::ERROR {0} -->", ex.Message);
            }
            return html;
        }
        private string GetIcon(RepositoryLocalObjectData obj)
        {
            var icon = string.Empty;
            try
            {
                var itemType = (int)new TcmUri(obj.Id).ItemType;
                var info = obj.VersionInfo as FullVersionInfo;
                var locked = 0;
                if (info != null)
                {
                    locked = (info.LockType.Value.HasFlag(LockType.CheckedOut)) ? 1 : 0;
                }
                var published = (obj.IsPublishedInContext == true) ? 1 : 0;
                icon = string.Format("T{0}L{1}P{2}", itemType, locked, published);
            }
            catch (Exception ex)
            {
                icon = string.Format("<!-- GetIcon::ERROR {0}:{1}  -->", ex.Message, ex.InnerException);
            }
            return icon;
        }
        /// <summary>
        /// Creates an HTML string containing the headings explaining our representation of a Tridion
        /// object's key information
        /// </summary>
        /// <returns>
        /// Formatted HTML presentation of the headings for key information for Tridion items
        /// </returns>
        public string CreateItemsHeading(){
            string html = "<div class=\"headings\">";
            html += "<div class=\"icon\">&nbsp</div>";
            html += "<div class=\"name\">Name</div>";
            html += "<div class=\"path\">Path</div>";
            html += "<div class=\"id\">ID</div></div>";
            
            return html;
        }

        /// <summary>
        /// Borrowed from Tridion.Web.UI.Core.Utils, this gets the current username to be used in 
        /// core service impersonation
        /// </summary>
        /// <returns>
        /// String containing the username
        /// </returns>
        public string GetUserName()
        {
            string text = string.Empty;
            if (HttpContext.Current != null && HttpContext.Current.User != null && HttpContext.Current.User.Identity != null)
            {
                text = HttpContext.Current.User.Identity.Name;
            }
            else if (ServiceSecurityContext.Current != null && ServiceSecurityContext.Current.WindowsIdentity != null)
            {
                text = ServiceSecurityContext.Current.WindowsIdentity.Name;
            }
            if (string.IsNullOrEmpty(text))
            {
                text = Thread.CurrentPrincipal.Identity.Name;
            }
            return text;
        }
    }
}