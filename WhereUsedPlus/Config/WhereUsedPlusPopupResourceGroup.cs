using Alchemy4Tridion.Plugins.GUI.Configuration;
using Alchemy4Tridion.Plugins.GUI.Configuration.Elements;

namespace WhereUsedPlus.Config
{
    /// <summary>
    /// Represents the ResourceGroup element within the editor configuration that contains this plugin's files
    /// and references.
    /// </summary>
    public class WhereUsedPlusPopupResourceGroup : ResourceGroup
    {
        /// <summary>
        /// Constructor
        /// </summary>
        public WhereUsedPlusPopupResourceGroup()
        {
            // When adding files you only need to specify the filename and not full path
            AddFile("WhereUsedPlusPopup.js");
            AddFile("WhereUsedPopup.css");
            // The above is just a convenient way of doing the following...
            // AddFile(FileTypes.Reference, "Alchemy.Plugins.HelloWorld.Commands.HelloCommandSet");

            // Since Alchemy comes with several libraries I can reference JQuery this way and avoid having
            // to add it myself
            Dependencies.AddLibraryJQuery();
            Dependencies.Add("Tridion.Web.UI.Editors.CME");
            Dependencies.Add("Tridion.Web.UI.Editors.CME.commands");
            
            // If you want this resource group to contain the js proxies to call your webservice, call AddWebApiProxy()
            AddWebApiProxy();

            // Let's add our resources to the WhereUsedPlusGroup.aspx page.  This will inject
            // the resources without us having to manually edit it.
            AttachToView("WhereUsedPlus.aspx");
        }
    }
}
