using Alchemy4Tridion.Plugins.GUI.Configuration;

namespace WhereUsedPlus.Config
{
    /// <summary>
    /// Represents a ribbon tool bar
    /// </summary>
    public class WhereUsedPlusRibbon : RibbonToolbarExtension
    {
        /// <summary>
        /// Constructor
        /// </summary>
        public WhereUsedPlusRibbon()
        {
            // The id of the element
            AssignId = "WhereUsedPlusBtn";

            // The filename of the ascx user control that contains the button markup/controls.
            //Group = "WhereUsedPlusGroup.ascx";

            // Using command instead of .ascx so we can position it correctly
            Command = "WhereUsedPlus";
            GroupId = Constants.GroupIds.HomePage.ManageGroup;
            InsertBefore = "WhereUsedBtn";

            // The name of the extension (used as label)
            Name = "Where Used Plus";
            // The title of the extenion (used as the title attribute of button)
            Title = "Where Used Plus";

            // Which Page tab the extension will go on.
            PageId = Constants.PageIds.HomePage;

            // Don't forget to add a dependency to the resource group that references the command set...
            Dependencies.Add<WhereUsedPlusResourceGroup>();

            // And apply it to a view.
            Apply.ToView(Constants.Views.DashboardView, "DashboardToolbar");
        }
    }
}
