﻿<!doctype html>
<html>
    <head runat="server">
        <title>Where Used Plus</title>
		<link rel='shortcut icon' type='image/x-icon' href='${ImgUrl}favicon.png' />
    </head>
    <body>
        <div class="tabs">
            <div class="active"></div>
        </div>
        <div class="tab-body active">
        </div>
		<div class="controls">
			<div class="button disabled" id="where_used_using"><span class="text">Where Used and Using</span></div>
			<div class="button disabled" id="pages_where_used"><span class="text">Pages Where Used</span></div>
			<div class="button disabled" id="open_item"><span class="text">Open</span></div>
			<div class="button disabled" id="go_to_item_location"><span class="text">Go To Location</span></div>
		</div>
    </body>
</html>